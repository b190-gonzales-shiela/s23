let myPlayer= {
	name: "Ash Ketchum",
	age: 10,
	pokemon: ["Pikachu", "Charizard", "Squirtle", "Bulbasaur"],
	friends:{
		hoenn: ["May", "Max"],
		kanto: ["Brock", "Misty"],
	},
	
	talk:function(){
		console.log("Hi I am Ash Ketchum");
	}

};
console.log(myPlayer);

console.log("Result of dot Notation:")
myPlayer.talk();

console.log("Result of square bracket notation")
myPlayer.talk();

console.log(myPlayer.pokemon);

let myPlayer1= {
	name: "Ash Ketchum",
	age: 10,
	
	talk:function(){
		console.log("Pikachu I choose you! ");
	}

};
console.log("Result of talk method")
myPlayer1.talk();

let myPokemon = {
	name: "Pikachu",
	level: 12,
	health: 24,
	attack: 12,
	tackle: function(){
		console.log( "Geodude tackled Pikachu" );
		console.log( "targetPokemon's health is now reduced to _targetPokemonHealth_" );
	},
	faint: function(){
		console.log("Pokemon Fainted")
	}
};
console.log(myPokemon);

let myPokemon1 = {
	name: "Geodude",
	level: 8,
	health: 16,
	attack: 8,
	tackle: function(){
		console.log( "This Pokemon tackled targetPokemon" );
		console.log( "Pikachu's health is now reduced to 16" );
	},
	faint: function(){
		console.log("Pokemon Fainted")
	}
};
console.log(myPokemon1);

let myPokemon2 = {
	name: "Mewtwo",
	level: 100,
	health: 200,
	attack: 100,
	tackle: function(){
		console.log( "This Pokemon tackled targetPokemon" );
		console.log( "Pikachu and Geodude's health is now reduced to 16" );
	},
	faint: function(){
		console.log("Pokemon Fainted")
	}
};
console.log(myPokemon2);


function Pokemon(name, level){
	// properties
	this.name=name;
	this.level=level;
	this.health=2*level;
	this.attack=level;

	// methods
	this.tackle=function(target){
		console.log(this.name+" " +"tackled"+" "+target.name);
		console.log("Pikachu's health is now reduced to 16");
	};
	this.faint=function(){
		console.log(this.name+"fainted.")
	}
}

let geodude=new Pokemon("Geodude",18);
let pikachu=new Pokemon("Pikachu", 20);


geodude.tackle(pikachu);

let myPokemon3 = {
	name: "Pikachu",
	level: 12,
	health: 16,
	attack: 12,
	tackle: function(){
	},
	faint: function(){
		console.log("Pokemon Fainted")
	}
};
console.log(myPokemon3);

function Pokemon(name, level){
	// properties
	this.name=name;
	this.level=level;
	this.health=2*level;
	this.attack=level;

	// methods
	this.tackle=function(target){
		console.log(this.name+" " +"tackled"+" "+target.name);
		console.log("Geodude's health is now reduced to -84");
	};
	this.faint=function(){
		console.log(this.name+" " +"fainted.")
	}
}

let mewtwo=new Pokemon("Mewtwo",100);
let geodude1=new Pokemon("Geodude", 8);


mewtwo.tackle(geodude1);
geodude1.faint(mewtwo);

let myPokemon4 = {
	name: "Geodude",
	level: 8,
	health: -84,
	attack: 8,
	tackle: function(){
		console.log( "This Pokemon tackled targetPokemon" );
		console.log( "Pikachu and Geodude's health is now reduced to 16" );
	},
	faint: function(){
		console.log("Pokemon Fainted")
	}
};
console.log(myPokemon4);


function Pokemon(name, level){
	// properties
	this.name=name;
	this.level=level;
	this.health=2*level;
	this.attack=level;

	// methods
	this.tackle=function(target){
		console.log(this.name+" " +"tackled"+" "+target.name);
		console.log("Pikachu's health is now reduced to -24");
	};
	this.faint=function(){
		console.log(this.name+" " +"fainted.")
	}
}

let mewtwo1=new Pokemon("Mewtwo",100);
let pickachu1=new Pokemon("Pikachu", 8);


mewtwo1.tackle(pickachu1);
pickachu1.faint(mewtwo1);


console.log("MEWTWO WINS!!")




